import { employer } from './../models/model.employee';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
   employees: employer[] = [

    {
      id: 1,
      nom : 'borel',
      email : 'borel@gmail.com',
      phone : 690603623
    },
    {
      id: 2,
      nom : 'pachy',
      email : 'pachy@gmail.com',
      phone : 690603623
    }

   ];

  constructor() { }

  onGet(){
     return this.employees ;
  }
  onAdd(employers: employer){
     this.employees.push(employers);

  }
  onDelete(id: number  ){

    //let employees = this.employees.find(x => x.id === id);

    let index = this.employees.findIndex(x => x.id === id);
    this.employees.splice(index, 1);
  }
  ongetedit(id: number){

   return this.employees.find(x => x.id === id) || new employer();
  }
  onUpdate(req: employer){
   const p: any = this.employees.find(x => x.id === req.id ) ;
   p.id = req.id;
   p.nom = req.nom;
   p.email = req.email;
   p.phone = req.phone


  }
}
