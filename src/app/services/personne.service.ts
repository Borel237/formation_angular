import { Personne } from './../personnes/personne';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PersonneService {
  personnes: Array<Personne> = new Array<Personne>();

  constructor() { }

 getAll(): Array<Personne> {
   return this.personnes;


 }
 addperson(p: Personne){
   
    return this.personnes.push(p);

 }
}
