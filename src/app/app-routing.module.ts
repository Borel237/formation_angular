import { ModifierComponent } from './pages/modifier/modifier.component';
import { EditEmployeeComponent } from './pages/edit-employee/edit-employee.component';
import { EmployeeComponent } from './employee/employee.component';
import { IndexComponent } from './index/index.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConnexionComponent } from './connexion/connexion.component';
import { FormulaireComponent } from './formulaire/formulaire.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { PersonneComponent } from './modules/cours/personne/personne.component';
import { DashbordComponent } from './dashbord/dashbord.component';

const routes: Routes = [

  { path: 'inscription', component: InscriptionComponent, },

  { path: 'connexion', component: ConnexionComponent, },

  { path:  'formulaire', component: FormulaireComponent, },

  { path:  'mode', component: PersonneComponent, },

  { path:  'index', component: IndexComponent, },

  { path:  'employer', component: EmployeeComponent, },

  { path:  'edit', component: EditEmployeeComponent, },

  { path:  'modifier/:id', component: ModifierComponent, },

  { path:  'dashbord', component: DashbordComponent, },


  { path: '**', redirectTo: 'inscription' }



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
