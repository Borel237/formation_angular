import { Personne } from './../../../personnes/personne';
import { PersonneService } from './../../../services/personne.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-personne',
  templateUrl: './personne.component.html',
  styleUrls: ['./personne.component.css']
})
export class PersonneComponent implements OnInit {

 personnes: Array <Personne> = new Array <Personne>();

  constructor(private personneService: PersonneService ) { }

  ngOnInit() {
    this.personnes = this.personneService.getAll();
  }

}
