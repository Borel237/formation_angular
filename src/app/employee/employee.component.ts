import { Component, OnInit } from '@angular/core';
import { employer } from '../models/model.employee';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  employee!: employer[];


  constructor(private employers:EmployeeService ) { }

  ngOnInit(): void {

    this.employee = this.employers.onGet();
  }
  onclick(id: any){

    console.log(id);

    this.employers.onDelete(id);
  }



}
