import { Router, RouterLinkActive, ActivatedRoute } from '@angular/router';
import { EmployeeService } from './../../services/employee.service';
import { employer } from './../../models/model.employee';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Builder } from 'selenium-webdriver';
import { group } from '@angular/animations';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {
  formulaire!: FormGroup;
  id!: number ;

  employer: employer = {
    id: 0 ,
    nom: '',
    email: '' ,
    phone: 0

  };




 constructor(
    private builder: FormBuilder,
    private employeeService: EmployeeService,
    private route: Router,
    private router: ActivatedRoute) { }

    ngOnInit(): void {

      this.o();

      this.id = +this.router.snapshot.params.get('id');


      this.employer = this.employeeService.ongetedit(this.id);

    }

   o(){
      this.formulaire = this.builder.group({

      id: ['', Validators.required],
      nom: ['', Validators.required],
      email: ['', Validators.email],
      phone: ['', Validators.required]
   });

 }


  onsubmit(){


    let resultat: employer ={

      id: this.formulaire.value.id,
      nom: this.formulaire.value.nom,
      email: this.formulaire.value.email,
      phone: this.formulaire.value.phone
    };

    this.employeeService.onAdd(resultat);
  }

}
