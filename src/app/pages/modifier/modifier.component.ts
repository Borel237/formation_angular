import { EmployeeService } from './../../services/employee.service';
import { employer } from './../../models/model.employee';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Builder } from 'selenium-webdriver';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';

@Component({
  selector: 'app-modifier',
  templateUrl: './modifier.component.html',
  styleUrls: ['./modifier.component.css']
})
export class ModifierComponent implements OnInit {

  id!: number;

   formulaire!: FormGroup ;

   employer!: employer;

   tab: employer = {
    id: 0,
    nom: '',
    email: '',
    phone: 0
  };

  constructor(
      private route: ActivatedRoute,
      private builder: FormBuilder,
      private employeeService:EmployeeService,
      private router: Router) {}

    formInit(tab: employer): void {
       this.formulaire = this.builder.group({
         id: [tab.id, Validators.required],
         nom: [tab.nom, Validators.required],
         email: [tab.email, Validators.required],
         phone: [tab.phone, Validators.required]
       });
    }



  ngOnInit(): void {

    this.id = Number(this.route.snapshot.paramMap.get('id'));

    this.tab = this.employeeService.ongetedit(this.id);

    this.formInit(this.tab);

    //console.log(this.tab);



  }
  onsubmit(){


    let resultat: employer ={

      id: this.formulaire.value.id,
      nom: this.formulaire.value.nom,
      email: this.formulaire.value.email,
      phone: this.formulaire.value.phone
    };
   // this.employeeService.onAdd(resultat);
    this.employeeService.onUpdate(resultat);
  }


}
