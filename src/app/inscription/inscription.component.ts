import { PersonneService } from './../services/personne.service';
import { Personne } from './../personnes/personne';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

personnes: Array<Personne> = new Array<Personne>();

personneForm = new FormGroup({
name: new FormControl('', [Validators.required], ) ,
tel: new FormControl('', [Validators.required], ) ,
pass: new FormControl('', [Validators.required])

});
  constructor(fb: FormBuilder, personneService: PersonneService) { }

  ngOnInit(): void {


  }
  get name(): AbstractControl | null {

     return this.personneForm.get('name');
  }
  onsubmit(){
    console.log(this.personneForm.value);
  }


}
