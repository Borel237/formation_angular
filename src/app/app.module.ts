import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { FormulaireComponent } from './formulaire/formulaire.component';
import { PersonneComponent } from './modules/cours/personne/personne.component';
import { IndexComponent } from './index/index.component';
import { EmployeeComponent } from './employee/employee.component';
import { EditEmployeeComponent } from './pages/edit-employee/edit-employee.component';
import { ModifierComponent } from './pages/modifier/modifier.component';
import { DashbordComponent } from './dashbord/dashbord.component';

@NgModule({
  declarations: [
    AppComponent,
    InscriptionComponent,
    ConnexionComponent,
    FormulaireComponent,
    PersonneComponent,
    IndexComponent,
    EmployeeComponent,
    EditEmployeeComponent,
    ModifierComponent,
    DashbordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
